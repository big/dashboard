app.service('serviceXhr', ["$http", function($http){
    this.post = function(url, params) {
        return $http.post(url, params)
            .then(function(response) {
                return response.data;
            })
            .catch(function (response) {
                throw response.data;
            });
    };
}]);
