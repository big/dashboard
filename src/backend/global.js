const fs = require("fs");
module.exports = function (app) {
    app.S4 = () => {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    app.guid = () => {
        return (app.S4() + app.S4() + "-" + app.S4() + "-" + app.S4() + "-" + app.S4() + "-" + app.S4() + app.S4() + app.S4());
    };

    app.log = (type, message) => {
        if (app.SETTINGS.debug) {
            console.log(message);
        }
        if (app.SETTINGS.logging.level.includes(type)) {
            try {
                message = type + ' - ' + new Date().toISOString() + ' : ' + message + '\n';
                fs.appendFileSync(`${app.locals.basedir}${app.SETTINGS.logging.path}/${app.SETTINGS.logging.file}`, message);
            } catch (err) {
                console.error(err);
            }
        }
    }

    app.getIp = (req) => {
        return req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.connection.remoteAddress;
    }
}
