let http = require('http');
let express = require('express');
const fs = require('fs');
const path = require("path");
let app = express();
const bodyParser = require('body-parser');

app.engine('pug', require('pug').__express)

app.set('views', __dirname + '/frontend/views');
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, "/")));
app.use(bodyParser.json({limit: '10mb'}));

app.locals.basedir = __dirname;

app.get("/", function (req, res)
{
    app.log("INFO", `${app.getIp(req)} - ${req.method} - ${req.url}`);
    res.render("index");
});

app.SETTINGS = JSON.parse(fs.readFileSync(__dirname + '/settings.json'));

if (!app.SETTINGS.ssc.enable) {
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0; // ignore self signed certificate
}

require("./backend/global.js")(app);

let serverPort = app.SETTINGS.server.port;
let serverHost = app.SETTINGS.server.host;

http.createServer(app).listen(serverPort, function(){
    app.log("INFO", "Express server listening on port " + serverPort);
    app.log("INFO", `http://${serverHost}:${serverPort}`);
});
